#!/bin/bash
while read USER PID RSS COMMAND; do
	if [[ $RSS -ge 1048576 ]]; then # print GB
		HUMGBPREC=`echo $RSS | awk '{ byte = $1 /1024/1024; print byte }'`
		HUMGBR=`printf "%.1f\n" $HUMGBPREC`
			echo $USER $PID "$HUMGBR"GB $COMMAND
	elif [[ $RSS -lt 1048576 ]] && [[ $RSS -ge 1024 ]]; then #print MB
		HUMMBPREC=`echo $RSS | awk '{ byte = $1 /1024; print byte }'`
		HUMMBR=`printf "%.1f\n" $HUMMBPREC`
			echo $USER $PID "$HUMMBR"MB $COMMAND
	else 
		echo $USER $PID "$RSS"KB $COMMAND

	fi
	
done < <(ps aux --sort -rss | head -n 16 | tail -n 15| awk '{print $1,$2,$6,$11}')
