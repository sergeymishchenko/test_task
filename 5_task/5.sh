#!/bin/bash
WHEREIAM=`pwd`
mkdir /mnt/archiso;
if [[ ! -f /tmp/archlinux-2018.06.01-x86_64.iso ]]; then 
	echo "We do not have an ISO image, downloading"
	wget ftp://ftp.byfly.by/pub/archlinux/iso/2018.06.01/archlinux-2018.06.01-x86_64.iso -O /tmp/archlinux-2018.06.01-x86_64.iso
else
	echo "It seems iso file is already present"
fi

mount -o loop /tmp/archlinux-2018.06.01-x86_64.iso /mnt/archiso; mkdir /mnt/customiso; rsync -avPh /mnt/archiso/ /mnt/customiso/; cd /mnt/customiso/arch/x86_64/ && unsquashfs /mnt/customiso/arch/x86_64/airootfs.sfs
mkdir /mnt/customiso/arch/x86_64/squashfs-root/root/.ssh; cat $WHEREIAM/.ssh/id_rsa.pub > /mnt/customiso/arch/x86_64/squashfs-root/root/.ssh/authorized_keys && chmod 600 /mnt/customiso/arch/x86_64/squashfs-root/root/.ssh/authorized_keys

chroot /mnt/customiso/arch/x86_64/squashfs-root /bin/bash -c "ln -s /usr/lib/systemd/system/sshd.service /etc/systemd/system/multi-user.target.wants/sshd.service"

rm -f /mnt/customiso/arch/x86_64/airootfs.sfs; mksquashfs /mnt/customiso/arch/x86_64/squashfs-root /mnt/customiso/arch/x86_64/airootfs.sfs; md5sum /mnt/customiso/arch/x86_64/airootfs.sfs > /mnt/customiso/arch/x86_64/airootfs.md5; cd /mnt/customiso; genisoimage -l -r -J -V "ARCH_201806" -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -c isolinux/boot.cat -o ../arch-custom.iso ./


