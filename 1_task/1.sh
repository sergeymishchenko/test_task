#!/bin/bash
LOGDIR="/var/log"
LOGRHEL="secure"
LOGDEB="auth.log"
LOGCUSTOM="./log_test"


if [[ -f $LOGDIR/$LOGDEB ]] ; then
	echo "This is Debian/Ubuntu - processing /var/log/auth.log"
	LOGSYS=$LOGDIR/$LOGDEB
elif [[ -f $LOGDIR/$LOGRHEL ]] ; then
	echo "This is RHEL - processing /var/log/secure"
	LOGSYS=$LOGDIR/$LOGRHEL
else 
	echo "give log file location to script like './1.sh %logname%'"
	LOGSYS="$LOGCUSTOM"
fi

while read QUANT SOURCE; do 
	if [[ $QUANT -gt 10 ]]; then
	echo "$SOURCE - block him!!!"
	`iptables -A INPUT -s $SOURCE -j DROP`
	fi
done < <(grep sshd $LOGSYS | egrep -i 'invalid[ ^I]user|authentication[ ^I]failure' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort | uniq -c | sort -n)

