#!/bin/bash
DATEBACKUP=`date +%Y%m%d`
DATEEXP=`date -d-1week +%Y%m%d`
LOG="/var/log/mybackup.log"
DESTDIR="/tmp/destdir"
SOURCEDIR="/tmp/sourcedir"
LASTFULLDATE=`find $DESTDIR -mindepth 1 -maxdepth 1 -name '????????.00.tgz' | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1 | sort | tail -n 1` #get date from last full like 20210101
LASTFULL=`find $DESTDIR -mindepth 1 -maxdepth 1 -name '????????.00.tgz' | rev | cut -d '/' -f 1 | rev` # get name like 20210101.00.tgz
LASTINCRNAME=`find $DESTDIR -mindepth 1 -maxdepth 1 -name '*.tgz' | rev | cut -d '/' -f 1 | rev | sort | tail -n 1`

if [[ $LASTFULLDATE -le $DATEEXP ]] || [[ ! -f $DESTDIR/$LASTFULL ]]; then
	echo "$DATEBACKUP new full copy starting" > $LOG
	echo "======================================" >> $LOG
	tar --numeric-owner -cvzpf "$DESTDIR/$DATEBACKUP.00.tgz" $SOURCEDIR -g "$DESTDIR/$DATEBACKUP.00.tgz.lst" 2>&1 >> $LOG ; EXITCODETAR=$? 
		if [[ $EXITCODETAR -eq 0 ]]; then
			echo "Backup ok" > /tmp/asd
		else
			echo "Backup failed" > /tmp/asd
		fi
	echo "$DATEBACKUP finished" >> $LOG
else
	NEWNUMBER=`basename $LASTINCRNAME | awk -F. '{printf "%.2d\n",$2+1}'`	
	echo "$DATEBACKUP $NEWNUMBER incremental starting" >> $LOG
	echo "======================================" >> $LOG
	cp "$DESTDIR/$DATEBACKUP.00.tgz.lst" "$DESTDIR/$DATEBACKUP.$NEWNUMBER.tgz.lst"
	tar --numeric-owner -cvzpf "$DESTDIR/$DATEBACKUP.$NEWNUMBER.tgz" $SOURCEDIR -g "$DESTDIR/$DATEBACKUP.$NEWNUMBER.tgz.lst" 2>&1 >> $LOG ; EXITCODETAR=$?
		if [[ $EXITCODE -eq 0 ]]; then
			echo "Backup ok" > /tmp/asd
                else
                        echo "Backup failed" > /tmp/asd
                fi
	echo "$DATEBACKUP $NEWNUMBER incremental finished" >> $LOG
fi
sendmail backupwatcher@backups.org < /tmp/asd 
